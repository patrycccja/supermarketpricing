package com.homework.pricing.logic;

import java.util.Currency;

import com.homework.pricing.logic.rounding.IRoundingStrategy;
import com.homework.pricing.logic.rounding.PerItemRoundingStrategy;

/**
 *
 */
public class GlobalConfig {
    private static Currency currency;
    private static IRoundingStrategy IRoundingStrategy = new PerItemRoundingStrategy();


    public static IRoundingStrategy getRoundingStrategy() {
        return IRoundingStrategy;
    }
}
