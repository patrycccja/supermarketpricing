package com.homework.pricing.logic.promo;

import java.util.Date;

/**
 * Base class defining promo strategy.
 */
public abstract class PromoStrategy implements IPromoStrategy {
    private Date startDate;
    private Date endDate;


    protected PromoStrategy(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
