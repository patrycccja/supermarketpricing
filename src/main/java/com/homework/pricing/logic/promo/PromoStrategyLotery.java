package com.homework.pricing.logic.promo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

import com.homework.pricing.common.MoneyCalc;
import com.homework.pricing.model.vo.TransactionVO;

/**
 * "If you're lucky you get 5% off"
 */
public class PromoStrategyLotery extends GlobalPromoStrategy {


    protected PromoStrategyLotery(Date startDate, Date endDate) {
        super(startDate, endDate);
    }

    @Override
    public Boolean isEntitledForPromo(TransactionVO transactionVO) {
        int rand = new Random().nextInt() % 10;
        return rand == 0;
    }

    @Override
    public BigDecimal getPrice(TransactionVO transactionVO) {
        if (!isEntitledForPromo(transactionVO)) {
            return transactionVO.getTotalPrice();
        }
        return MoneyCalc.getPercentage(transactionVO.getTotalPrice(), 95);
    }

}
