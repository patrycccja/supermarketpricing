package com.homework.pricing.logic.promo;

import java.math.BigDecimal;

import com.homework.pricing.model.vo.Item;

/**
 *
 */
public class NoPromoStrategy implements IProdPromoStrategy {

    @Override
    public Boolean isEntitledForPromo(Item item, int numberOfItemsOrUnits) {
        return true;
    }

    @Override
    public BigDecimal getPrice(Item item, int numberOfItemsOrUnitsBought) {
        return item.calculateRegularPrice(numberOfItemsOrUnitsBought);
    }
}
