package com.homework.pricing.logic.promo;

import java.math.BigDecimal;
import java.util.Date;

import com.homework.pricing.common.MoneyCalc;
import com.homework.pricing.model.vo.Item;
import com.homework.pricing.model.vo.ProductByWeightVO;
import com.homework.pricing.model.vo.ProductVO;

/**
 * "Buy <pre>numberOfPromoItems</pre> for Ł<pre>promoFixedPrice</pre>" price schema.
 * Eg. "buy 3 for Ł1.50"
 *  or "buy 300g of cheese for Ł14.40"
 */
public class PromoStrategyFixedMultibuy extends ProdPromoStrategy {
    private BigDecimal promoFixedPrice;

    public PromoStrategyFixedMultibuy(Date startDate, Date endDate, Integer quantityForPromo, BigDecimal promoFixedPrice) {
        super(startDate, endDate, quantityForPromo);
        this.promoFixedPrice = promoFixedPrice;
    }


    @Override
    public BigDecimal getPrice(Item item, int numberOfItemsOrUnits) {
        int nofOutOfPromo = numberOfItemsOrUnits % getNumberOfItemsOrUnitsForPromo();
        int nofInPromo = numberOfItemsOrUnits - nofOutOfPromo;
        if (item instanceof ProductByWeightVO) {
            return getPrice(item.getRegularPrice(), ((ProductByWeightVO)item).getNofUnits(), nofInPromo, nofOutOfPromo);
        } else if (item instanceof ProductVO) {
            return getPrice(item.getRegularPrice(), nofInPromo, nofOutOfPromo);
        }
        return null;
    }

    public BigDecimal getPrice(BigDecimal regularPrice, int nofInPromo, int nofOutOfPromo) {
        return MoneyCalc.sum(
                MoneyCalc.multi( nofInPromo / getNumberOfItemsOrUnitsForPromo(), promoFixedPrice ),
                MoneyCalc.multi( nofOutOfPromo, regularPrice )
        );
    }

    public BigDecimal getPrice(BigDecimal regularPrice, Integer nofUnitsForRegPrice, int nofInPromo,
                               int nofOutOfPromo) {
        return MoneyCalc.sum(
                MoneyCalc.multi( nofInPromo / getNumberOfItemsOrUnitsForPromo(), promoFixedPrice ),
                MoneyCalc.multi( nofOutOfPromo/ nofUnitsForRegPrice, regularPrice )
        );
    }


    protected BigDecimal getPromoFixedPrice() {
        return promoFixedPrice;
    }
}
