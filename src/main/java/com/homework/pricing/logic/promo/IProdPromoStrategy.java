package com.homework.pricing.logic.promo;

import java.math.BigDecimal;

import com.homework.pricing.model.vo.Item;

/**
 *
 */
public interface IProdPromoStrategy {

    public Boolean isEntitledForPromo(Item item, int numberOfItemsOrUnits);

    public BigDecimal getPrice(Item item, int numberOfItemsOrUnits);
}
