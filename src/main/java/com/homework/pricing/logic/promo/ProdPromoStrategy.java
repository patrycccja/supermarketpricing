package com.homework.pricing.logic.promo;

import java.math.BigDecimal;
import java.util.Date;

import com.homework.pricing.model.vo.Item;

/**
 *
 */
public abstract class ProdPromoStrategy extends PromoStrategy implements IProdPromoStrategy {
    /**
     * Number of items or units that entitle customer to use a promo offer. For products sold by weight it means a weight in units
     * defined by {@link com.homework.pricing.model.vo.ProductByWeightVO#getUnit()}
     */
    private Integer numberOfItemsOrUnitsForPromo;
    // private Integer maxNofPromoPerClient = 1; // how many times one client can use certain promotion (per each type of the
    // product)


    protected ProdPromoStrategy(Date startDate, Date endDate, Integer numberOfItemsOrUnitsForPromo) {
        super(startDate, endDate);
        this.numberOfItemsOrUnitsForPromo = numberOfItemsOrUnitsForPromo;
    }


    public Boolean isEntitledForPromo(Item item, int numberOfItemsOrUnits) {
        return numberOfItemsOrUnits > numberOfItemsOrUnitsForPromo;
    }

    public abstract BigDecimal getPrice(Item item, int numberOfItemsOrUnits);

    protected Integer getNumberOfItemsOrUnitsForPromo() {
        return numberOfItemsOrUnitsForPromo;
    }

}
