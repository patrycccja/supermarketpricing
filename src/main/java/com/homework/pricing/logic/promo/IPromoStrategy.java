package com.homework.pricing.logic.promo;

import java.util.Date;

/**
 *
 */
public interface IPromoStrategy {

    public Date getStartDate();

    public Date getEndDate();
}
