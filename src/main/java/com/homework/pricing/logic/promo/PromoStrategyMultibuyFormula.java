package com.homework.pricing.logic.promo;

import java.math.BigDecimal;
import java.util.Date;

import com.homework.pricing.common.MoneyCalc;
import com.homework.pricing.model.vo.Item;
import com.homework.pricing.model.vo.ProductByWeightVO;

/**
 * "Buy <pre>quantityForPromo</pre> items, get <pre>numberOfFreeItems</pre> for free" price schema.
 * Eg. "buy 5 items, get 2 for free" = "5 for the price of 3"
 */
public class PromoStrategyMultibuyFormula extends ProdPromoStrategy {
    private int numberOfFreeItems;


    public PromoStrategyMultibuyFormula(Date startDate, Date endDate, Integer quantityForPromo, int numberOfFreeItems) {
        super(startDate, endDate, quantityForPromo);
        this.numberOfFreeItems = numberOfFreeItems;
    }


    @Override
    public BigDecimal getPrice(Item item, int numberOfItemsOrUnits) {
        int freeItemsOrUnitsInTotal = numberOfItemsOrUnits / getNumberOfItemsOrUnitsForPromo() * getNumberOfFreeItems();
        int paidItemsOrUnitsInTotal = numberOfItemsOrUnits - freeItemsOrUnitsInTotal;
        if (item instanceof ProductByWeightVO) {
            Integer nofUnitsForRegPrice = ((ProductByWeightVO)item).getNofUnits();
            return MoneyCalc.multi( paidItemsOrUnitsInTotal/nofUnitsForRegPrice, item.getRegularPrice() );
        } else {
            return MoneyCalc.multi( paidItemsOrUnitsInTotal, item.getRegularPrice() );
        }
    }

    protected int getNumberOfFreeItems() {
        return numberOfFreeItems;
    }
}
