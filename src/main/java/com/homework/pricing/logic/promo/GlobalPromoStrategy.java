package com.homework.pricing.logic.promo;

import java.math.BigDecimal;
import java.util.Date;

import com.homework.pricing.model.vo.TransactionVO;

/**
 *
 */
public abstract class GlobalPromoStrategy extends PromoStrategy {

    protected GlobalPromoStrategy(Date startDate, Date endDate) {
        super(startDate, endDate);
    }

    public abstract Boolean isEntitledForPromo(TransactionVO transactionVO);

    public abstract BigDecimal getPrice(TransactionVO transactionVO);
}
