package com.homework.pricing.logic.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.homework.pricing.common.MoneyCalc;
import com.homework.pricing.logic.GlobalConfig;
import com.homework.pricing.logic.promo.IProdPromoStrategy;
import com.homework.pricing.logic.promo.IPromoStrategy;
import com.homework.pricing.logic.rounding.IRoundingStrategy;
import com.homework.pricing.model.vo.BasketItemVO;
import com.homework.pricing.model.vo.BasketVO;
import com.homework.pricing.model.vo.Item;
import com.homework.pricing.model.vo.PaymentMethod;
import com.homework.pricing.model.vo.ProductByWeightVO;
import com.homework.pricing.model.vo.TransactionVO;

/**
 * Client's transaction.
 */
public class TransactionService {

    // Yeah. It shouldn't be static. Neither the rest of serv. methods.
    public static TransactionVO performTransaction(BasketVO basketVO, PaymentMethod paymentMethod) {
        TransactionVO transactionVO = new TransactionVO(paymentMethod, basketVO);
        checkout(transactionVO);
        return transactionVO;
    }

    private static void checkout(TransactionVO transactionVO) {
        transactionVO.endTransaction( calculateTotalPrice(transactionVO) );
    }

    // this method should do much more: register operations performed - which promo schema was used for which product,
    // what was the regular price and what was the paid price of each product so that detailed receipt or report could be
    // created.
    // TansactionVO should keep it next to the basket. This way Tesco-style receipt ;) could be printed.
    private static BigDecimal calculateTotalPrice(TransactionVO transactionVO) {
        IRoundingStrategy roundingStrategy = GlobalConfig.getRoundingStrategy();
        BigDecimal total = BigDecimal.ZERO;
        if (transactionVO.getBasketVO()==null || transactionVO.getBasketVO()==null
                || transactionVO.getBasketVO().getProductsSummary().isEmpty()) {
            return total;
        }
        for (BasketItemVO biVO : transactionVO.getBasketVO().getProductsSummary()) {
            BigDecimal productTotalPrice = SupermarketService.getTodaysPromo(biVO.getItem())
                    .getPrice( biVO.getItem(), biVO.getNumberOfItemsOrUnits() );
            total = MoneyCalc.sum( total, roundingStrategy.roundPerItemPrice(productTotalPrice) );
        }
        return roundingStrategy.roundTotalPrice(total);
    }

}
