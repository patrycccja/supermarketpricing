package com.homework.pricing.logic.service;

import com.homework.pricing.model.vo.BasketItemVO;
import com.homework.pricing.model.vo.BasketVO;
import com.homework.pricing.model.vo.Item;

/**
 *
 */
public class BasketService {
    private static Integer idSeq = 1;

    /** let's say it's an add() method */
    public static BasketVO createBasket() {
        BasketVO basketVO = new BasketVO(idSeq++);
        return basketVO;
    }

    public static boolean addProducts(BasketVO basketVO, Item item, int numberOfItemsOrUnits) throws IllegalArgumentException {
        if (basketVO==null) {
            throw new IllegalArgumentException("Basket must not be null");
        }
        if (item==null) {
            throw new IllegalArgumentException("Item must not be null");
        }
        return basketVO.addItem(new BasketItemVO(item, numberOfItemsOrUnits));
    }



}
