package com.homework.pricing.logic.service;

import java.util.HashMap;

import com.homework.pricing.logic.promo.IProdPromoStrategy;
import com.homework.pricing.logic.promo.IPromoStrategy;
import com.homework.pricing.logic.promo.NoPromoStrategy;
import com.homework.pricing.model.vo.Item;

/**
 * This is class that knows everything ;) Here is our knowledge regarding the supermarket, products and promos.
 */
public class SupermarketService {
    /**
     * Let's say that on daily basis these two in-memory maps are refreshed. Or not.... Maybe there is different requirement.
     * Maybe after data is loaded to the memory it's updated each time sth is changed (and persisted at the same time). Or
     * maybe the supermarket is so huge, that its data can not be stored in-memory even if we want to store only some basic data
     * (including name, price and promotions) and the rest (additional descriptions and stuff) is loaded when product's details
     * need to be displayed. So maybe it's retrieved from a sql DB/ nosql DB/ cache each time. Or maybe we have some RESTful
     * services... Maybe. Now it's kept in-memory :)
     */
    private static HashMap<Integer, Item> allProductsavailable = new HashMap<>();
    private static HashMap<Integer, IProdPromoStrategy> todaysPromotions = new HashMap<>();
    private static IProdPromoStrategy noPromo = new NoPromoStrategy();

    public static void defineProducts(HashMap<Integer, Item> products) {
        allProductsavailable = products;
    }

    public static void defineTodaysPromotions(HashMap<Integer, IProdPromoStrategy> promotions) {
        todaysPromotions = promotions;
    }

    /**
     * I don't know how to identify products since not all of them have a barcode. So for the sake of this model I assume that
     * somehow product's id is received (based on the barcode, the name or whatever else) and then it's used to get product's
     * details.
     */
    public static Item getProductDetails(Integer productId) {
       return allProductsavailable.get(productId);
    }

    /**
     * Returns today's promotion applicable for provided item or null if there are no promos available.
     */
    //TODO Future features: each item can have more promos assigned (also: Item can be assigned to a producer/brand and a
    // category (or multiple categories). Each of them can have its own promos defined.)
    // Then we should create a chain of PromoStrategy-s + define how to handle them. Because usually promos do not add up. So
    // for example each strategy can return info if it was applied. Also: we can define which promo is more important - the one
    // defined for the item or the one that is more general (brand/category promo).
    public static IProdPromoStrategy getTodaysPromo(Item item) {
        return todaysPromotions.containsKey(item.getId()) ? todaysPromotions.get(item.getId()) : noPromo;
    }
}
