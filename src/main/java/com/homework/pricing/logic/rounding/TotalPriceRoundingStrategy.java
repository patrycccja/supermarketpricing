package com.homework.pricing.logic.rounding;

import java.math.BigDecimal;

import com.homework.pricing.common.MoneyCalc;

/**
 *
 */
public class TotalPriceRoundingStrategy implements IRoundingStrategy {

    @Override
    public BigDecimal roundPerItemPrice(BigDecimal price) {
        return price;
    }

    @Override
    public BigDecimal roundTotalPrice(BigDecimal price) {
        return MoneyCalc.round(price);
    }
}
