package com.homework.pricing.logic.rounding;

import java.math.BigDecimal;

/**
 * This is strategy to define how the price rounding should be handled. Meaning WHEN it should happen.
 */
public interface IRoundingStrategy {
    public BigDecimal roundPerItemPrice(BigDecimal price);
    public BigDecimal roundTotalPrice(BigDecimal price);
}
