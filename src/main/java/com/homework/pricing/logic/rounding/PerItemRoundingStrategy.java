package com.homework.pricing.logic.rounding;

import java.math.BigDecimal;

import com.homework.pricing.common.MoneyCalc;

/**
 *
 */
public class PerItemRoundingStrategy implements IRoundingStrategy {

    @Override
    public BigDecimal roundPerItemPrice(BigDecimal price) {
        return MoneyCalc.round(price);
    }

    @Override
    public BigDecimal roundTotalPrice(BigDecimal price) {
        return price;
    }
}
