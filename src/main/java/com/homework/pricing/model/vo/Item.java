package com.homework.pricing.model.vo;

import java.math.BigDecimal;

/**
 *
 */
//TODO Future features: Item can be assigned to a producer/brand and also a category (or multiple categories). And... and each
// producer, brand and category can have its own promos defined... All of it should be handled by a SupermarketService
public interface Item {
    Integer getId();
    String getName();
    BigDecimal getRegularPrice();
    BigDecimal calculateRegularPrice(int numberOfItemsOrUnits);
    ItemUnit getUnit();


    public static enum ItemUnit {
        PIECE, G, KG
    }

}
