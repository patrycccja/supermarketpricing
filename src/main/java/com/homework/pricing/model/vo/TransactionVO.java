package com.homework.pricing.model.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Client's transaction.
 */
public class TransactionVO {
    private final Date transactionDate;
    private final PaymentMethod paymentMethod;
    private final BasketVO basketVO;
    private BigDecimal totalPrice;

    public TransactionVO(PaymentMethod paymentMethod, BasketVO basketVO) {
        this.transactionDate = new Date();
        this.paymentMethod = paymentMethod;
        this.basketVO = basketVO;
    }

    public TransactionVO(Date transactionDate, PaymentMethod paymentMethod, BasketVO basketVO) {
        this.transactionDate = transactionDate;
        this.paymentMethod = paymentMethod;
        this.basketVO = basketVO;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public BasketVO getBasketVO() {
        return basketVO;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void endTransaction(BigDecimal totalPrice) {
        if (totalPrice == null) {
            throw new IllegalArgumentException("Total price can NOT be null");
        }
        if ( totalPrice.compareTo(new BigDecimal(0)) < 0 ) {
            throw new IllegalArgumentException("Total price less than zero? How is that even possible?");
        }
        basketVO.setCheckedOut(true);
        this.totalPrice = totalPrice;
    }
}
