package com.homework.pricing.model.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class BasketVO {
    private final Integer id;
    // ... some client-related stuff etc. ...
    private final Date created;
    /**
     * List of products kept in the same order they were added to the basket.
     * Note: the same product can be added multiple times!
     */
    // If we need list of products that is arranged differently (eg. simple summary or a list sorted by category) it can always
    // be built from the detailed list of items.
    // + ~Event driven arch.
    private List<BasketItemVO> items = new ArrayList<>();
    /**
     * checkedOut == true means that transaction was created for the basked, client has already paid for what he bought,
     * hence the basket can NOT be altered in any way anymore.
     */
    private Boolean checkedOut = false;


    public BasketVO(Integer id, Date created) {
        this.id = id;
        this.created = created;
    }

    public BasketVO(Integer id) {
        this(id, new Date());
    }


    public Integer getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

    public Boolean getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(Boolean checkedOut) {
        if (checkedOut == false) {
            this.checkedOut = checkedOut;
        }
    }

    public boolean addItem(BasketItemVO item) throws IllegalStateException {
        if (checkedOut != false) {
            throw new IllegalStateException("Basked is already closed. Adding more items is not allowed.");
        }
        return items.add(item);
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns a list of products with quantity of each ot the products.
     */
    // and what if we need additional "transformed" object other than BasketItemVO - where should we add it?  Inner class???
    public Collection<BasketItemVO> getProductsSummary() {
        Map<Integer, BasketItemVO> summary = new HashMap<>(); // item.id => BasketItemVO for this item
        Integer key;
        for (BasketItemVO vo : items) {
            key = vo.getItem().getId();
            if (summary.containsKey(key)) {
                summary.put( key, summary.get(key).increaseNumberOfItemsOrUnits(vo.getNumberOfItemsOrUnits()) );
            } else {
                summary.put(key, vo);
            }
        }
        return summary.values();
    }

}
