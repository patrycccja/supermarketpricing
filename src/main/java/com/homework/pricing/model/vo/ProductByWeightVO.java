package com.homework.pricing.model.vo;

import java.math.BigDecimal;

import com.homework.pricing.common.MoneyCalc;

/**
 * Product in a supermarket that is sold by weight.
 * Exemplary values:
 * - name = "Smelly cheese", regularPrice = 1.99, nofUnits = 100, quantityUnit=ItemUnit.G
 * which means it costs Ł1.99 per 100g.
 */
public class ProductByWeightVO extends ProductVO {
    /** number of units */
    private final Integer nofUnits;
    //TODO Consider: maybe define the minimal amount (nofUnits) that can be bought? What if someone wants 1g of cheese ;) From
    // the other side... does it need formal constraints?


    public ProductByWeightVO(Integer id, String name, BigDecimal regularPrice, Integer nofUnits, ItemUnit unit, String barcode) {
        super(id, name, regularPrice, barcode, unit);
        this.nofUnits = nofUnits;
    }

    public ProductByWeightVO(Integer id, String name, BigDecimal regularPrice, Integer nofUnits, ItemUnit unit) {
        this(id, name, regularPrice, nofUnits, unit, null);
    }


    public Integer getNofUnits() {
        return nofUnits;
    }

    @Override
    public BigDecimal calculateRegularPrice(int numberOfItemsOrUnits) {
        return MoneyCalc.multi(numberOfItemsOrUnits / getNofUnits(), getRegularPrice());
    }
}
