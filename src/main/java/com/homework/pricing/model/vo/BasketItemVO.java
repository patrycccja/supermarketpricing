package com.homework.pricing.model.vo;

public final class BasketItemVO {
    private final Item item;
    private final Integer numberOfItemsOrUnits;

    public BasketItemVO(Item item, Integer numberOfItemsOrUnits) {
        this.item = item;
        this.numberOfItemsOrUnits = numberOfItemsOrUnits;
    }

    public Item getItem() {
        return item;
    }

    public Integer getNumberOfItemsOrUnits() {
        return numberOfItemsOrUnits;
    }

    /**
     * Returns new object whose item is the same to this item's, but number of items/units is increased by provided value.
     */
    public BasketItemVO increaseNumberOfItemsOrUnits(Integer byNumber) {
        return new BasketItemVO(item, this.numberOfItemsOrUnits + byNumber);
    }

}
