package com.homework.pricing.model.vo;

import java.math.BigDecimal;

import com.homework.pricing.common.MoneyCalc;

/**
 * Product in a supermarket.
 * Exemplary values:
 * - name = "Six-pack of Juicy Apple Juice", regularPrice = 6.50, unit=ItemUnit.PIECE
 * which means it costs Ł6.50 a piece.
 */
//TODO Feature for the future: regularPrice can change in time. We should be able to change it and keep a track of changes
// (event driven dev? :)

public class ProductVO implements Item {
    private final Integer id;
    private final String barcode; // not everything has a barcode...
    private final String name;
    private final BigDecimal regularPrice;
    private final ItemUnit unit;


    public ProductVO(Integer id, String name, BigDecimal regularPrice, String barcode) {
        this(id, name, regularPrice, barcode, ItemUnit.PIECE);
    }

    public ProductVO(Integer id, String name, BigDecimal regularPrice) {
        this(id, name, regularPrice, null, ItemUnit.PIECE);
    }

    ProductVO(Integer id, String name, BigDecimal regularPrice, String barcode, ItemUnit unit) {
        this.id = id;
        this.name = name;
        this.regularPrice = regularPrice;
        this.barcode = barcode;
        this.unit = unit;
    }


    @Override
    public Integer getId() {
        return id;
    }

    public String getBarcode() {
        return barcode;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getRegularPrice() {
        return regularPrice;
    }

    @Override
    public BigDecimal calculateRegularPrice(int numberOfItemsOrUnits) {
        return MoneyCalc.multi(getRegularPrice(), numberOfItemsOrUnits);
    }

    @Override
    public ItemUnit getUnit() {
        return unit;
    }
}
