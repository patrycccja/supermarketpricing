package com.homework.pricing.model.vo;

public enum PaymentMethod {
    CASH,
    DEBIT_CARD,
    CREDIT_CARD,
    PROMO_COUPONS
}
