package com.homework.pricing.common;

import java.math.BigDecimal;

// TODO For the future : check out Joda-Money - opensource, v. 0.10

public final class MoneyCalc {
    private static int ROUNDING_MODE = BigDecimal.ROUND_HALF_EVEN;
    private static int ROUNDING_SCALE = 2;
    private static BigDecimal S100 = new BigDecimal("100");

    public static BigDecimal sum(BigDecimal x, BigDecimal y) {
        return x.add(y);
    }

    public static BigDecimal multi(BigDecimal x, Integer y) {
        return x.multiply(new BigDecimal(y));
    }

    public static BigDecimal multi(Integer y, BigDecimal x) {
        return x.multiply(new BigDecimal(y));
    }

    public static BigDecimal subtract(BigDecimal x, BigDecimal y) {
        return x.subtract(y);
    }

    public static BigDecimal diff(BigDecimal x, BigDecimal y) {
        return subtract(x, y);
    }

    public static BigDecimal getPercentage(BigDecimal x, Integer percentage) {
        return getPercentage(x, new BigDecimal(percentage));
    }

    public static BigDecimal getPercentage(BigDecimal x, BigDecimal percentage) {
        BigDecimal result = x.multiply(percentage);
        result = result.divide(S100, ROUNDING_MODE);
        return round(result);
    }

    public static BigDecimal round(BigDecimal x) {
        return x.setScale(ROUNDING_SCALE, ROUNDING_MODE);
    }
}
