package com.homework.pricing.logic.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import com.homework.pricing.common.MoneyCalc;
import com.homework.pricing.logic.promo.IProdPromoStrategy;
import com.homework.pricing.logic.promo.IPromoStrategy;
import com.homework.pricing.logic.promo.PromoStrategyFixedMultibuy;
import com.homework.pricing.logic.promo.PromoStrategyMultibuyFormula;
import com.homework.pricing.model.vo.BasketVO;
import com.homework.pricing.model.vo.Item;
import com.homework.pricing.model.vo.PaymentMethod;
import com.homework.pricing.model.vo.ProductByWeightVO;
import com.homework.pricing.model.vo.ProductVO;
import com.homework.pricing.model.vo.TransactionVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TransactionServiceTest {
    private static Integer idSeq;
    private static Integer firstPrdSoldIndividually;
    private static Integer nofItems;
    private HashMap<Integer, Item> products = new HashMap<>();
    private HashMap<Integer, IProdPromoStrategy> promotions = new HashMap<>();

    @Before
    public void before() {
        idSeq = 1;

        Item i = new ProductByWeightVO(idSeq++, "cheese", new BigDecimal(1.99), 100, Item.ItemUnit.G);
        addProd(i, null);
        i = new ProductByWeightVO(idSeq++, "tomatoes", new BigDecimal(1.99), 1, Item.ItemUnit.KG);
        IProdPromoStrategy s = new PromoStrategyFixedMultibuy(new Date(), new Date(), 3, new BigDecimal(2.3)); // Ł2.3 for 3kg
        addProd(i, s);
        i = new ProductByWeightVO(idSeq++, "potato", new BigDecimal(0.50), 1, Item.ItemUnit.KG);
        s = new PromoStrategyMultibuyFormula(new Date(), new Date(), 5, 2); // buy 5kg of potatoes, pay for 3!
        addProd(i, s);

        firstPrdSoldIndividually = idSeq;

        i = new ProductVO(idSeq++, "apple", new BigDecimal(0.4));
        s = new PromoStrategyFixedMultibuy(new Date(), new Date(), 3, new BigDecimal(1));
        addProd(i, s);
        i = new ProductVO(idSeq++, "bread", new BigDecimal(1.25));
        addProd(i, null);
        i = new ProductVO(idSeq++, "fizzy drink", new BigDecimal(0.7));
        s = new PromoStrategyMultibuyFormula(new Date(), new Date(), 3, 1);
        addProd(i, s);

        nofItems = idSeq - 1;
        SupermarketService.defineProducts(products);
        SupermarketService.defineTodaysPromotions(promotions);
    }

    private void addProd(Item i, IProdPromoStrategy s) {
        products.put(i.getId(), i);
        if (s != null) {
            promotions.put(i.getId(), s);
        }
    }

    @Test
    public void testSingleProducts() {
        testMultiBasket(1, new BigDecimal(4.34));
    }

    @Test
    public void testTripleBasket() {
        testMultiBasket(3, new BigDecimal(12.12));
    }

    @Test
    public void testQuadrupleBasket() {
        testMultiBasket(4, new BigDecimal(16.46));
    }

    @Test
    public void testPrdByWeight() {
        BasketVO b = BasketService.createBasket();
        BasketService.addProducts(b, products.get(1), 100); // cheese
        BasketService.addProducts(b, products.get(2), 1); // tomatoes, 1kg
        BasketService.addProducts(b, products.get(3), 1); // potatoes, 1kg

        Assert.assertNotNull(b.getProductsSummary());
        Assert.assertEquals(3, b.getProductsSummary().size());
        BigDecimal total = getTotalPrice(b);
        Assert.assertEquals(MoneyCalc.round(new BigDecimal(4.48)), total);

        BasketService.addProducts(b, products.get(2), 3); // tomatoes, +3kg
        BasketService.addProducts(b, products.get(3), 6); // potatoes, +6kg

        Assert.assertEquals(3, b.getProductsSummary().size());
        total = getTotalPrice(b);
        Assert.assertEquals(MoneyCalc.round(new BigDecimal(8.78)), total);
    }

    private void testMultiBasket(int multipleProdsBy, BigDecimal expectedPrice) {
        BasketVO b = BasketService.createBasket();
        for (int i=0; i<multipleProdsBy; i++) {
            addProductsToBasket(b);
        }

        Assert.assertNotNull(b.getProductsSummary());
        Assert.assertEquals((long)nofItems-firstPrdSoldIndividually+2, b.getProductsSummary().size());
        BigDecimal total = getTotalPrice(b);
        Assert.assertEquals(MoneyCalc.round(expectedPrice), total);
    }

    private void addProductsToBasket(BasketVO b) {
        BasketService.addProducts(b, products.get(1), 100); // cheese
        for (int i=firstPrdSoldIndividually; i<=nofItems; i++) { // no cheese
            BasketService.addProducts(b, products.get(i), 1);
        }
    }

    private BigDecimal getTotalPrice(BasketVO b) {
        TransactionVO t = TransactionService.performTransaction(b, PaymentMethod.CASH);
        return t.getTotalPrice();
    }
}
